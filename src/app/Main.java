package app;

import java.util.Random;
import java.util.Scanner;

import core.Animal;
import core.Protectora;

public class Main {

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		int capacidadMaxima = 10;
		String nombreProtectora = "animal-protect";
		Protectora refuegio = new Protectora(nombreProtectora, capacidadMaxima);
		
		
		Random rnd = new Random();
		while(!refugio.isFull()) {
			
			System.out.println("Introduce el nombre del animal");
			String nombreAnimal = teclado.nextLine();
			
			Animal animal;
			int typeAnimal = rnd.nextInt(2);
			if(typeAnimal == 0) {
				animal = new Gato(nombreAnimal);
			}else {
				animal = new Perro(nombreAnimal);
			}
			refuegio.add(animal);
		}
		System.out.println(refugio);
	}
}
